package pt.ipb.dts.cicd.dtscicd.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.ipb.dts.cicd.dtscicd.entities.Note;

@Repository
public interface NoteRepository
        extends JpaRepository<Note, Integer> {
}
