package pt.ipb.dts.cicd.dtscicd.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.ipb.dts.cicd.dtscicd.entities.Note;
import pt.ipb.dts.cicd.dtscicd.repo.NoteRepository;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("api/note")
@CrossOrigin(origins = "http://localhost:7070")
public class NoteController {

    private final NoteRepository noteRepository;

    public NoteController(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @GetMapping
    public List<Note> getFoo() {
        return noteRepository.findAll();
    }

    @PostMapping
    public Note post(@RequestBody String content) {
        Note note = new Note();
        note.setContent(content);
        note.setCreateDate(LocalDateTime.now());
        return noteRepository.save(note);
    }


}
